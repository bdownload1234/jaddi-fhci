$(document).ready(function() {

  // Banner Carousel
  const bannerSwiper = new Swiper('#banner .banner-swiper', {
    direction: 'horizontal',
    effect: "fade",
    loop: true,
    navigation: {
      nextEl: '.banner-navigation-next',
      prevEl: '.banner-navigation-prev',
    },
  });

  bannerSwiper.on('slideChange', function (e) {
    $("#banner .banner-swiper video").map(e => { $("#banner .banner-swiper video")[e].pause() });
    setTimeout(() => {
      let checkHasVideo = $("#banner .banner-swiper .banner-slide.swiper-slide.swiper-slide-visible.swiper-slide-active video");
      if(checkHasVideo.length > 0) {
        checkHasVideo[0].play();
      }
    }, 500);
  });
  // End Banner Carousel

  // Section Swiper Carousel
  const programUnggulanSwiper = new Swiper('#section-program-unggulan .section-swiper', {
    direction: 'horizontal',
    loop: false,
    slidesPerView: 2,
    spaceBetween: 36,
    centeredSlides: true,
    navigation: {
      nextEl: ".swiper-button-next.section-navigation-next",
      prevEl: ".swiper-button-prev.section-navigation-prev",
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
        spaceBetween: 40,
        initialSlide : 1,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 36,
        initialSlide : 1,
      },
    },
  });
  // End Section Swiper Carousel

  // Toggle modal play video
  $('#playVideo').on('shown.bs.modal', function() {
    $(this).find('video')[0].play();
  });

  $('#playVideo').on('hidden.bs.modal', function() {
    $(this).find('video')[0].pause();
  });
  // End Toggle modal play video

  // Section Swiper Carousel
  const magazineSwiper = new Swiper('#section-magazine .section-swiper', {
    direction: 'horizontal',
    loop: true,
    slidesPerView: 2,
    spaceBetween: 16,
    centeredSlides: true,
    navigation: {
      nextEl: ".swiper-button-next.section-navigation-next",
      prevEl: ".swiper-button-prev.section-navigation-prev",
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
        initialSlide : 1,
      },
      1024: {
        slidesPerView: 6,
        initialSlide : 1,
      },
    },
  });
  // End Section Swiper Carousel

  // Section Swiper Carousel
  const commiteeSwiper = new Swiper('#section-commitee .section-swiper', {
    direction: 'horizontal',
    loop: false,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false,
    },
    slidesPerView: 1,
    spaceBetween: 16,
    centeredSlides: true,
    navigation: {
      nextEl: ".swiper-button-next.section-navigation-next",
      prevEl: ".swiper-button-prev.section-navigation-prev",
    },
  });
  // End Section Swiper Carousel
  
  // Toggle change bg navbar on scroll
  $(document).on('scroll', function() {
    if($(this).scrollTop() > 300) {
      $('nav.navbar').addClass('navbar-light bg-white');
      $('nav.navbar .nav-link').addClass('text-dark');
      $('nav.navbar .navbar-brand img').attr('src', 'assets/JADDI+/Logo/logo-color.png');
    } else {
      if(page == 'index') {
        $('nav.navbar').removeClass('navbar-light bg-white');
        $('nav.navbar .nav-link').removeClass('text-dark');
        $('nav.navbar .navbar-brand img').attr('src', 'assets/JADDI+/Logo/logo-white.png');
      } else {
        $('nav.navbar').removeClass('navbar-light bg-white');
        $('nav.navbar .nav-link').removeClass('text-dark');
        $('nav.navbar .navbar-brand img').attr('src', 'assets/JADDI+/Logo/logo-color.png');
      }
    }
  });
  // End Toggle change bg navbar on scroll

  
})